import { EventEmitter, Component, Output } from "@angular/core";
import { ILogger } from "../services/ILogger";
import { BugSearchService } from "./services/bugsearch.service";

@Component({
    selector:'search-bar',
    templateUrl: './searchbar.component.html'
})
export class SearchBar {
    //@Output()
    //onNewSearcKey=new EventEmitter<string>();

    constructor(public logger: ILogger, public bugSearchService : BugSearchService) {
    }
    onSearchkeyChange(evt) {
        var key = evt.target.value;
        this.logger.writeLog('Search key: ' + key);
        //this.onNewSearcKey.emit(key);
        this.bugSearchService.searchByLevel(key);
    }
}