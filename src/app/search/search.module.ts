import { NgModule } from "@angular/core";
import { SearchPanel } from "./searchpanel.component";
import { SearchResult } from "./searchresult.component";
import { SearchBar } from "./searchbar.component";
import { CommonModule } from "@angular/common";

@NgModule({
    declarations: [
        SearchResult,
        SearchBar,
        SearchPanel
    ],
    imports: [
        CommonModule
    ],
    exports: [
        SearchPanel
    ]
})
export class SearchModule {

}