import { Component, Input, ChangeDetectorRef } from "@angular/core";
import { BugSearchService } from "./services/bugsearch.service";

@Component({
    selector:'search-result',
    templateUrl: './searchresult.component.html'
})
export class SearchResult {

    private _cdRef : ChangeDetectorRef;
    constructor(public bugsearchservice : BugSearchService, cdRef : ChangeDetectorRef) {
        this.bugsearchservice.searchResultObservableSource.subscribe(
            this.onSearchResultSubjectStreamChange
        );
        this._cdRef = cdRef;
    }
    onSearchResultSubjectStreamChange(payload){
        this.searchResults = payload;
        console.log(payload);
        this._cdRef.markForCheck();
    }

    /* Readonly 
    pure component - Never modified inside component
        - Sent from parent.
    */
    //@Input()
    searchResults : any;
}