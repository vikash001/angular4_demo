import { Component } from "@angular/core";
import { ILogger } from "../services/ILogger";
import { BugSearchService } from "./services/bugsearch.service";

@Component({
    selector:'search-panel',
    templateUrl: './searchpanel.component.html',
    providers: [
        {
            provide: BugSearchService,
            useClass: BugSearchService
        }
    ]
})
export class SearchPanel {

    private _logger: ILogger;
    private _bugSearchService : BugSearchService;
    private searchResults : any;
    constructor(logger : ILogger, bugSearchServie : BugSearchService) {
        this._logger = logger;
        this._bugSearchService = bugSearchServie;
    }

    onNewSearchKeyHandler(val) {
        this._logger.writeLog('Search panel modified : ' + val);
        this.searchResults = this._bugSearchService.searchByLevel(val);
        this._logger.writeLog(JSON.stringify(this.searchResults));
    }
}