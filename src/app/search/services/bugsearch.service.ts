import { Subject } from 'rxjs';

export class BugSearchService {

    private searchResultSource = new Subject();
    searchResultObservableSource = this.searchResultSource.asObservable();
    bugList = new Array();
    constructor(){
        this.bugList.push({ bugId: 'B001', description: 'new Bug1', timestamp:new Date(), status:'Open', level:'Low' });
        this.bugList.push({ bugId: 'B002', description: 'new Bug2', timestamp:new Date(), status:'Open', level:'Medium' });
        this.bugList.push({ bugId: 'B005', description: 'new Bug5', timestamp:new Date(), status:'Open', level:'Low' });
        this.bugList.push({ bugId: 'B003', description: 'new Bug3', timestamp:new Date(), status:'Open', level:'High' });
        this.bugList.push({ bugId: 'B004', description: 'new Bug4', timestamp:new Date(), status:'Closed', level:'Medium' });
    }

    searchByLevel(level) {
        let resultList = [];
        for(var i =0; i < this.bugList.length; i ++) {
            if(this.bugList[i].level == level) {
                resultList.push(this.bugList[i]); 
                //resultList.push(Object.create(this.bugList[i]));
            }            
        }
        // new item added to stream
        this.searchResultSource.next(resultList);
        //return resultList;
    }
}