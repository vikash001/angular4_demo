export abstract class IAccountsRepository {
    abstract authenticate(userName:string, password:string) : boolean; 
}