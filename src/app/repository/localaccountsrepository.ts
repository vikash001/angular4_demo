import { UserModel } from "../models/usermodel";
import { IAccountsRepository } from "./IAccountsRepository";

export class LocalAccountsRepository extends IAccountsRepository {

    constructor() {
        super();
        this.userList.push(new UserModel('admin', 'admin', 'admin@pic.com'));
        this.userList.push(new UserModel('tom', 'tom123', 'tom@pic.com'));
        this.userList.push(new UserModel('pat', 'pat123', 'pat@pic.com'));
        this.userList.push({ userName: 'vikash', password: 'vikash', email:'vikash@pic.com'});
    }
    userList = new Array<UserModel>();
    authenticate (userName, password) {
        for(var i = 0; i < this.userList.length; i++)
        {
            if(this.userList[i].userName == userName && this.userList[i].password == password) {
                return true;
            }
        }
        return false;
    }
}