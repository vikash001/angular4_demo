import { ILogger } from './ILogger';

export class FileLogger extends ILogger {
    constructor() {
        super();
        console.log("Console logger instantiated.");
    }
    writeLog(msg){
        console.log(msg);
    }
}