import { ILogger } from './ILogger';

export class ConsoleLogger extends ILogger {
    writeLog(msg){
        console.log(msg);
    }
}