import { IAccountsRepository } from "../repository/IAccountsRepository";
import { Inject, Injectable } from "@angular/core";
import { ILogger } from "./ILogger";

@Injectable()
export class AccountsService {
    private _logger : ILogger
    constructor(@Inject(IAccountsRepository) public repository : IAccountsRepository,
        logger : ILogger) {
            this._logger = logger;
    }

    validate(userName, password) :boolean {
        this._logger.writeLog('Authenticating');
        return this.repository.authenticate(userName, password);
    }
}