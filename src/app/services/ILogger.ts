
export abstract class ILogger {
    abstract writeLog(msg : string) : void;
}