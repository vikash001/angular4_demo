import { BrowserModule } from '@angular/platform-browser';
import { RootComponent } from './root.component';
import { NgModule } from '@angular/core';
import { LayoutModule } from './layout/layout.module';
import { AccountsModule } from './accounts/accounts.module';
import { ILogger } from './services/ILogger';
import { ConsoleLogger } from './services/consolelogger.service';
import { FileLogger } from './services/filelogger.service';
import { IAccountsRepository } from './repository/IAccountsRepository';
import { LocalAccountsRepository } from './repository/localaccountsrepository';
import { AccountsService } from './services/accountsservice.service';
import { SearchModule } from './search/search.module';

@NgModule({
    declarations : [
        RootComponent
    ],
    bootstrap : [
        RootComponent
    ],
    imports : [
        BrowserModule, 
        LayoutModule,
        AccountsModule,
        SearchModule
    ],
    providers : [
        {
            //provide:ConsoleLogger,
            provide: ILogger, //'logger',
            //useClass:ConsoleLogger
            useValue: new FileLogger()
            //useValue: new ConsoleLogger()
            //useFactory: function() {
            //    return new ConsoleLogger();
            //}
        },
        {
            provide: IAccountsRepository,
            useClass: LocalAccountsRepository
        },
        {
            provide: AccountsService,
            useClass : AccountsService
        }
    ]
})
export class AppModule {

}