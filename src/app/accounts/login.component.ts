import { Component } from "@angular/core";
import { Inject } from '@angular/core';
import { ILogger } from '../services/ILogger';
import { AccountsService } from "../services/accountsservice.service";

@Component({
    selector: 'login-comp',
    templateUrl: './login.component.html'
})
export class LoginComponent {

    _logger : ILogger;
    private _accountservice : AccountsService;
    constructor( logger : ILogger, accountservice : AccountsService) { //@Inject('logger')
        this._logger = logger;
        this._accountservice = accountservice;
    }

    // Properties
    userName : string = "Enter user name";
    password : string = "Enter password";
    status : string;

    // Bahaviors
    login(){
        if(this._accountservice.validate(this.userName, this.password))
        {
            this.status = "Valid credentials.";
        } else {
            this.status = "Invalid credentials.";
        }
        //if(this.userName=="admin" && this.password=="admin") {
        //    this.status = "Valid credentials.";
        //} else {
        //    this.status = "Invalid credentials.";
        //}
    }

    reset(){
        this.userName = "";
        this.password = "";
        this.status = "";
    }

    onUserNameEdit(userNameValue:string){
        this.userName = userNameValue;
    }

    onPasswordEdit(passwordValue:string) {
        this.password = passwordValue;
    }
}
