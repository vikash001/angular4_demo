import { NgModule } from "@angular/core";
import { LoginComponent } from "./login.component";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SignupComponent } from "./signup.component";
import { CommonModule } from '@angular/common';


@NgModule({
    declarations:[
        LoginComponent,
        SignupComponent
    ],
    imports:[
        FormsModule,
        ReactiveFormsModule,
        CommonModule
    ],
    exports:[
        LoginComponent,
        SignupComponent
    ]
})
export class AccountsModule {

}