import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl } from '@angular/forms';

@Component({
    selector: 'signup-comp',
    templateUrl: './signup.component.1.html'
})
export class SignupComponent { //implements OnInit
    
    //form: FormGroup;

    // Life-cycle Hook
    //ngOnInit() {
    //    this.form = new FormGroup({
    //        name : new FormControl(),
    //        password : new FormControl(),
    //        email : new FormControl()
    //    });
    //}

    //signUp() {
    //    console.log(this.form);
    //}

    //reset() {
    //    this.form.reset();
    //}


    form : FormGroup;
    signUp(formRef) {
        this.form = formRef;
        console.log(formRef);
    }

    reset() {
        this.form.reset();
    }
}