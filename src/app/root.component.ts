import { Component } from "@angular/core";

@Component({
    selector: 'root-comp',
    template: `<div><h3>RootComponent Content</h3>
                <header-comp></header-comp>
                <login-comp></login-comp>
                <footer-comp></footer-comp>
            </div>
            <hr/>
            <div>
                <search-panel></search-panel>
            </div>
            <hr/>
            <div>
                <signup-comp></signup-comp>
            </div>`
})
export class RootComponent {

}